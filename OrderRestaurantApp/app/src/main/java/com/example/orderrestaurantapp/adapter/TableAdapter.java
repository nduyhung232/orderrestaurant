package com.example.orderrestaurantapp.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.model.Table;

import java.util.List;

/**
 * Created by duyhung on 25/01/2018.
 */

public class TableAdapter extends ArrayAdapter {
    Activity context;
    int resource;
    List<Table> objects;

    public TableAdapter(@NonNull Activity context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View view = inflater.inflate(this.resource, null);
        Table table = this.objects.get(position);

        TextView roomName = view.findViewById(R.id.name);
        roomName.setText(table.getName());

        Animation animation;
        animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        animation.setDuration(1400);
        view.startAnimation(animation);

        return view;
    }
}
