package com.example.orderrestaurantapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Table implements Serializable {
    private int id;
    private int roomId;
    private String name;
    private ArrayList listOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getListOrder() {
        return listOrder;
    }

    public void setListOrder(ArrayList listOrder) {
        this.listOrder = listOrder;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public Table(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Table(int id, int roomId, String name) {
        this.id = id;
        this.roomId = roomId;
        this.name = name;
    }
}
