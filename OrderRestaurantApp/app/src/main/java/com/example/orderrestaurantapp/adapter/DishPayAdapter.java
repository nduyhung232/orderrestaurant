package com.example.orderrestaurantapp.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.model.Dish;
import com.example.orderrestaurantapp.utility.ConvertMoneyDisplay;

import java.util.List;

/**
 * Created by duyhung on 25/01/2018.
 */

public class DishPayAdapter extends ArrayAdapter {

    Activity context;
    int resource;
    List<Dish> objects;

    public DishPayAdapter(@NonNull Activity context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // convert money
        ConvertMoneyDisplay convertMoneyDisplay = new ConvertMoneyDisplay();

        LayoutInflater inflater = this.context.getLayoutInflater();
        View view = inflater.inflate(this.resource, null);

        Dish dish = this.objects.get(position);

        ImageView img = view.findViewById(R.id.img);
        TextView txtQuantity = view.findViewById(R.id.txtQuantity);
        TextView txtDishName = view.findViewById(R.id.txtDishName);
        TextView txtPrice = view.findViewById(R.id.txtPrice);

        String quantity = String.valueOf(dish.getQuantity());
        String price = convertMoneyDisplay.convert(dish.getPrice());
        txtDishName.setText(dish.getName());
        txtQuantity.setText(quantity);
        txtPrice.setText(price);

        if (dish.getImage() != null) {
            byte[] decodedString = Base64.decode(dish.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            img.setImageBitmap(decodedByte);
        }

        Animation animation;
        animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        animation.setDuration(1400);
        view.startAnimation(animation);

        return view;
    }
}
