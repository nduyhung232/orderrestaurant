package com.example.orderrestaurantapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.orderrestaurantapp.calback.CallBack;
import com.example.orderrestaurantapp.calback.CallBackString;
import com.example.orderrestaurantapp.calback.CallBackInt;
import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.adapter.DishMenuAdapter;
import com.example.orderrestaurantapp.adapter.DishOrderAdapter;
import com.example.orderrestaurantapp.dialog.ConfirmDialog;
import com.example.orderrestaurantapp.dialog.QuantityDialog;
import com.example.orderrestaurantapp.model.Dish;
import com.example.orderrestaurantapp.model.Order;
import com.example.orderrestaurantapp.model.Table;
import com.example.orderrestaurantapp.service.GetMethod;
import com.example.orderrestaurantapp.service.PostMethod;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.orderrestaurantapp.Config.createOrUpdateOrder;
import static com.example.orderrestaurantapp.Config.getCurrentOrder;
import static com.example.orderrestaurantapp.Config.getMenu;
import static com.example.orderrestaurantapp.Config.linkServer;

public class OrderActivity extends AppCompatActivity {
    private Button btnOrder;
    private Button btnPay;

    private ListView lvMenu;
    private ArrayList<Dish> menuList;
    private DishMenuAdapter menuAdapter;

    private ListView lvOrder;
    private ArrayList<Dish> orderList;
    private DishOrderAdapter dishOrderAdapter;

    private Table currentTable;
    private Order currentOrder;

    private SharedPreferences sharedPreferences;
    private TextView txtTitle;
    private TextView txtStatus;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Intent intent = getIntent();
        currentTable = (Table) intent.getSerializableExtra("table");

        addControlls();
        addEvents();
    }

    private void addControlls() {
        lvMenu = findViewById(R.id.lvMenu);
        lvOrder = findViewById(R.id.lvOrder);
        txtTitle = findViewById(R.id.txtTitle);
        txtStatus = findViewById(R.id.txtStatus);
        btnOrder = findViewById(R.id.btnOrder);
        btnPay = findViewById(R.id.btnPay);
        progressBar = findViewById(R.id.progressBar);

        /*
                Set title
         */
        txtTitle.setText(currentTable.getName());

        /*
                Get Menu
         */
        new GetMethod(new CallBackString() {
            @Override
            public void doit(String str) {
                if (str == null) {

                } else {
                    menuList = new Gson().fromJson(str, new TypeToken<List<Dish>>() {
                    }.getType());
                    menuAdapter = new DishMenuAdapter(
                            OrderActivity.this,
                            R.layout.item_dish_menu,
                            menuList);
                    lvMenu.setAdapter(menuAdapter);
                }

                getCurrentOrderListView();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, linkServer + getMenu);


    }

    private void getCurrentOrderListView() {
        /*
                Get CurrentOrder
         */
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", currentTable.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new PostMethod(new CallBackString() {
            @Override
            public void doit(String str) {
                currentOrder = new Gson().fromJson(str, new TypeToken<Order>() {
                }.getType());

                /*
                    convert menu to currentOder
                 */
                orderList = new ArrayList<>();
                for (Dish dish : currentOrder.getDishList()) {
                    int idDish = dish.getId() - 1;
                    Dish dish1 = menuList.get(idDish);
                    dish1.setTypeFood(dish.getTypeFood());
                    dish1.setQuantity(dish.getQuantity());

                    orderList.add(dish1);
                }
                currentOrder.getDishList().clear();
                currentOrder.getDishList().addAll(orderList);

                dishOrderAdapter = new DishOrderAdapter(
                        OrderActivity.this,
                        R.layout.item_dish_order,
                        orderList);
                lvOrder.setAdapter(dishOrderAdapter);
                /*
                    Gone Progress Bar
                 */
                progressBar.setVisibility(View.GONE);

                /*
                    Set Status Table
                */
                if (currentOrder.getStatus() == 0) {
                    txtStatus.setText("Bàn Trống");
                    txtStatus.setTextColor(Color.parseColor("#FF4FB112"));
                } else if (currentOrder.getStatus() == 1) {
                    txtStatus.setText("Đang Đợi Đồ");
                    txtStatus.setTextColor(Color.parseColor("#FFFF0000"));
                } else if (currentOrder.getStatus() == 2) {
                    txtStatus.setText("Đã Lên Đồ");
                    txtStatus.setTextColor(Color.parseColor("#FFFFFFFF"));
                }
            }
        }).execute(linkServer + getCurrentOrder, jsonObject.toString());
    }

    private void addEvents() {
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

                new QuantityDialog().newInstance(new CallBackInt() {
                    @Override
                    public void doit(int a) {
                        Dish dish = (Dish) parent.getItemAtPosition(position);
                        dish.setQuantity(a);

                        orderList.add(dish);
                        dishOrderAdapter.notifyDataSetChanged();
                    }
                }).show(getFragmentManager(), "");
            }
        });

        lvOrder.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                new ConfirmDialog().newInstance(new CallBack() {
                    @Override
                    public void doit() {
                        orderList.remove(position);
                        dishOrderAdapter.notifyDataSetChanged();
                    }
                }).show(getFragmentManager(), "");


                return false;
            }
        });


        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                    if order list == null -> toast
                */
                if (orderList.size() == 0) {
                    Toast.makeText(OrderActivity.this, "Danh sách order trống", Toast.LENGTH_LONG).show();
                } else {

                    JSONArray jsonArray = new JSONArray();
                    for (Dish dish : orderList) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("id", dish.getId());
                            jsonObject.put("name", dish.getName());
                            jsonObject.put("price", dish.getPrice());
                            jsonObject.put("typeFood", dish.getTypeFood());
                            jsonObject.put("quantity", dish.getQuantity());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    // Create order
                    sharedPreferences = getSharedPreferences("account", MODE_PRIVATE);
                    int accountID = sharedPreferences.getInt("id", 0);

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("id", currentOrder.getId());
                        jsonObject.put("staffId", accountID);
                        jsonObject.put("tableId", currentTable.getId());
                        jsonObject.put("status", 1);
                        jsonObject.put("dishList", jsonArray);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // format jsonArray
                    String order_send = jsonObject.toString();
                    order_send = order_send.replaceAll("\"\\[", "[");
                    order_send = order_send.replaceAll("]\"", "]");
                    order_send = order_send.replaceAll("\\\\", "");

                    new PostMethod(new CallBackString() {
                        @Override
                        public void doit(String str) {
                            Toast.makeText(OrderActivity.this, "Order thành công", Toast.LENGTH_LONG).show();
                            txtStatus.setText("Đang Đợi Đồ");

                            currentOrder = new Gson().fromJson(str, new TypeToken<Order>() {
                            }.getType());

                            /*
                                convert menu to currentOder
                             */
                            orderList = new ArrayList<>();
                            for (Dish dish : currentOrder.getDishList()) {
                                int idDish = dish.getId() - 1;
                                Dish dish1 = menuList.get(idDish);
                                dish1.setTypeFood(dish.getTypeFood());
                                dish1.setQuantity(dish.getQuantity());

                                orderList.add(dish1);
                            }
                            currentOrder.getDishList().clear();
                            currentOrder.getDishList().addAll(orderList);

                            dishOrderAdapter = new DishOrderAdapter(
                                    OrderActivity.this,
                                    R.layout.item_dish_order,
                                    orderList);
                            lvOrder.setAdapter(dishOrderAdapter);

                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, linkServer + createOrUpdateOrder, order_send);
                }
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, PayActivity.class);
                intent.putExtra("currentOrder", currentOrder);
                intent.putExtra("currentTable", currentTable);

                startActivity(intent);
            }
        });
    }
}
