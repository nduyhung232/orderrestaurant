package com.example.orderrestaurantapp.utility;

import java.text.NumberFormat;
import java.util.Locale;

public class ConvertMoneyDisplay {
    public String convert(float input){

        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currencyVN = NumberFormat.getCurrencyInstance(localeVN);
        String output = currencyVN.format(input);

        return output;
    }
}
