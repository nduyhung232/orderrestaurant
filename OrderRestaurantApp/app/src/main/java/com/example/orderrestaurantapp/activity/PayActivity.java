package com.example.orderrestaurantapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.adapter.DishPayAdapter;
import com.example.orderrestaurantapp.calback.CallBackInt;
import com.example.orderrestaurantapp.calback.CallBackString;
import com.example.orderrestaurantapp.dialog.AccountNumber_Dialog;
import com.example.orderrestaurantapp.dialog.MoneyDialog;
import com.example.orderrestaurantapp.model.Dish;
import com.example.orderrestaurantapp.model.Order;
import com.example.orderrestaurantapp.calback.CallBackFloat;
import com.example.orderrestaurantapp.model.Table;
import com.example.orderrestaurantapp.service.PostMethod;
import com.example.orderrestaurantapp.utility.ConvertMoneyDisplay;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.orderrestaurantapp.Config.checkOut;
import static com.example.orderrestaurantapp.Config.linkServer;
import static com.example.orderrestaurantapp.Config.transfer;

public class PayActivity extends AppCompatActivity {
    private Order currentOrder;
    private Table currentTable;
    private float totalDue;

    private TextView txtTotalDue;
    private TextView txtPay;
    private TextView txtRefunds;
    private TextView txtTitle;
    private Button btnCheckout_byMoney, btnCheckout_byCard;

    private ListView lvDishPay;
    private DishPayAdapter dishPayAdapter;
    private ArrayList<Dish> dishArrayList;

    private ConvertMoneyDisplay convertMoneyDisplay;
    private float refunds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        // convert monet
        convertMoneyDisplay = new ConvertMoneyDisplay();

        Intent intent = getIntent();
        currentOrder = (Order) intent.getSerializableExtra("currentOrder");
        currentTable = (Table) intent.getSerializableExtra("currentTable");

        addControlls();
        addEvents();
    }

    private void addControlls() {
        txtTotalDue = findViewById(R.id.txtTotalDue);
        txtTitle = findViewById(R.id.txtTitle);
        txtPay = findViewById(R.id.txtKhachTra);
        txtRefunds = findViewById(R.id.txtTraLai);
        btnCheckout_byMoney = findViewById(R.id.btnCheckout_byMoney);
        btnCheckout_byCard = findViewById(R.id.btnCheckout_byCard);

        txtTitle.setText(currentTable.getName());

        lvDishPay = findViewById(R.id.lvDishPay);
        dishArrayList = currentOrder.getDishList();

        dishPayAdapter = new DishPayAdapter(
                PayActivity.this,
                R.layout.item_dish_pay,
                dishArrayList
        );
        lvDishPay.setAdapter(dishPayAdapter);

        for (Dish value : currentOrder.getDishList()) {
            totalDue += value.getPrice() * value.getQuantity();
        }
        txtTotalDue.setText(convertMoneyDisplay.convert(totalDue));
    }

    private void addEvents() {
        txtPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MoneyDialog().newInstance(new CallBackFloat() {
                    @Override
                    public void doit(float a) {
                        refunds = a - totalDue;
                        txtPay.setText(convertMoneyDisplay.convert(a));
                        txtRefunds.setText(convertMoneyDisplay.convert(refunds));
                        if (refunds < 0) {
                            Toast.makeText(PayActivity.this, "Khách trả thiếu tiền", Toast.LENGTH_LONG).show();
                            txtPay.setTextColor(Color.RED);
                            txtRefunds.setTextColor(Color.RED);
                        } else {
                            txtPay.setTextColor(Color.BLACK);
                            txtRefunds.setTextColor(Color.BLACK);
                        }
                    }
                }).show(getFragmentManager(), null);
            }
        });

        btnCheckout_byMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (refunds <= 0) {
                    Toast.makeText(PayActivity.this, "Khách trả thiếu tiền", Toast.LENGTH_LONG).show();
                } else {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("id", currentOrder.getId());
                        jsonObject.put("timeOrder", totalDue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new PostMethod(new CallBackString() {
                        @Override
                        public void doit(String str) {
                            Toast.makeText(PayActivity.this, "Thanh Toán Thành Công", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PayActivity.this, TableActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }).execute(linkServer + checkOut, jsonObject.toString());
                }
            }
        });

        btnCheckout_byCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AccountNumber_Dialog().newInstance(new CallBackInt() {
                    @Override
                    public void doit(int a) {
                        JSONObject jsonObject2 = new JSONObject();
                        try {
                            jsonObject2.put("account_from", a);
                            jsonObject2.put("account_to", 111);
                            jsonObject2.put("amount", totalDue);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        new PostMethod(new CallBackString() {
                            @Override
                            public void doit(String str) {

                                if (str.equals("true")) {
                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put("id", currentOrder.getId());
                                        jsonObject.put("timeOrder", totalDue);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    new PostMethod(new CallBackString() {
                                        @Override
                                        public void doit(String str) {
                                            Toast.makeText(PayActivity.this, "Thanh Toán Thành Công", Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(PayActivity.this, TableActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }).execute(linkServer + checkOut, jsonObject.toString());
                                } else {
                                    Toast.makeText(PayActivity.this, "Số Tài Khoản Không Tồn Tại Hoặc Số Dư Không Đủ", Toast.LENGTH_LONG).show();
                                }
                            }
                        }).execute(linkServer + transfer, jsonObject2.toString());


                    }
                }).show(getFragmentManager(), "");

            }
        });
    }
}
