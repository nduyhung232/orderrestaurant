package com.example.orderrestaurantapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.orderrestaurantapp.calback.CallBackString;
import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.adapter.RoomAdapter;
import com.example.orderrestaurantapp.adapter.TableAdapter;
import com.example.orderrestaurantapp.model.Room;
import com.example.orderrestaurantapp.model.Table;
import com.example.orderrestaurantapp.service.GetMethod;
import com.example.orderrestaurantapp.service.PostMethod;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static com.example.orderrestaurantapp.Config.getListRoom;
import static com.example.orderrestaurantapp.Config.getListTableInRoom;
import static com.example.orderrestaurantapp.Config.linkServer;

public class TableActivity extends AppCompatActivity {
    private View hView;
    private DrawerLayout drawer;

    private ListView lvRoom;
    private ArrayList<Room> roomList;
    private RoomAdapter roomAdapter;

    private ListView lvTable;
    private ArrayList<Table> tableList;
    private TableAdapter tableAdapter;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        addControlls();
        addEvents();
    }

    private void addControlls() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        hView = navigationView.getHeaderView(0);
        drawer = findViewById(R.id.drawer_layout);
        sharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE);

        lvRoom = findViewById(R.id.lvGroup);
        lvTable = findViewById(R.id.lvTable);
        roomList = new ArrayList<>();
        tableList = new ArrayList<>();

        // get List Room
        new GetMethod(new CallBackString() {
            @Override
            public void doit(String str) {
                if (str == null) {

                } else {
                    roomList = new Gson().fromJson(str, new TypeToken<List<Room>>() {
                    }.getType());
                    roomAdapter = new RoomAdapter(
                            TableActivity.this,
                            R.layout.item_room_layout,
                            roomList);
                    lvRoom.setAdapter(roomAdapter);
                }
            }
        }).execute(linkServer + getListRoom);

        // get List Table
        Room room = new Room();
        room.setId(1);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String roomJson = gson.toJson(room);
        new PostMethod(new CallBackString() {
            @Override
            public void doit(String str) {
                tableList = new Gson().fromJson(str, new TypeToken<List<Table>>() {
                }.getType());

                tableAdapter = new TableAdapter(
                        TableActivity.this,
                        R.layout.item_table_layout,
                        tableList);
                lvTable.setAdapter(tableAdapter);

            }
        }).execute(linkServer + getListTableInRoom, roomJson);
    }

    private void addEvents() {
        lvRoom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Room room = (Room) parent.getItemAtPosition(position);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String roomJson = gson.toJson(room);

                // get List Table
                new PostMethod(new CallBackString() {
                    @Override
                    public void doit(String str) {
                        tableList = new Gson().fromJson(str, new TypeToken<List<Table>>() {
                        }.getType());

                        tableAdapter = new TableAdapter(
                                TableActivity.this,
                                R.layout.item_table_layout,
                                tableList);
                        lvTable.setAdapter(tableAdapter);

                    }
                }).execute(linkServer + getListTableInRoom, roomJson);
                drawer.closeDrawer(GravityCompat.START);

                setTitle(room.getName());
            }
        });

        lvTable.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Table table = (Table) parent.getItemAtPosition(position);
                Intent intent = new Intent(TableActivity.this, OrderActivity.class);
                intent.putExtra("table", table);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.table, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_signout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("userName");
            editor.remove("token");
            editor.apply();

            Intent intent = new Intent(TableActivity.this, LoginActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
