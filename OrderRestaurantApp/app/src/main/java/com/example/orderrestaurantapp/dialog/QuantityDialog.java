package com.example.orderrestaurantapp.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.orderrestaurantapp.calback.CallBackInt;
import com.example.orderrestaurantapp.R;

public class QuantityDialog extends DialogFragment {
    CallBackInt callBack;

    public QuantityDialog newInstance(CallBackInt callBack) {
        QuantityDialog quantityDialog = new QuantityDialog();
        quantityDialog.callBack = callBack;
        return quantityDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.quantity_dialog, null);

        final EditText editQuantity = view.findViewById(R.id.editQuantity);

        Button btnCancel = view.findViewById(R.id.btnCancel);
        Button btnOk = view.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editQuantity.getText().toString().trim().equals("")) {

                } else {
                    int quantity = Integer.parseInt(editQuantity.getText().toString().trim());
                    callBack.doit(quantity);
                    dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }
}
