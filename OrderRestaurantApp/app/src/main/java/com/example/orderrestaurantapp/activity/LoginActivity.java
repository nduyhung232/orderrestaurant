package com.example.orderrestaurantapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.orderrestaurantapp.calback.CallBackString;
import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.model.Account;
import com.example.orderrestaurantapp.service.PostMethod;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.orderrestaurantapp.Config.checkToken;
import static com.example.orderrestaurantapp.Config.getLogin;
import static com.example.orderrestaurantapp.Config.linkServer;

public class LoginActivity extends AppCompatActivity {

    private EditText editUserName, editPassWord;
    private Button btnLogin;
    private SharedPreferences sharedPreferences;
    private Animation anim_LeftToRight, anim_RightToLeft, anim_ZoomIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        btnLogin = findViewById(R.id.btnLogin);

        addControlls();
        // check internet connection
        if (isConnected() == false) {
            Toast.makeText(LoginActivity.this, "Không có kết nối mạng", Toast.LENGTH_SHORT).show();
            btnLogin.setEnabled(false);
        } else {
            TextView noConnection = findViewById(R.id.connection);
            noConnection.setVisibility(View.GONE);
            checkToken();
        }

        addAnima();
        addEvents();
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    private void addControlls() {
        editUserName = findViewById(R.id.editUsername);
        editPassWord = findViewById(R.id.editPassword);
        anim_LeftToRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_to_right);
    }

    private void checkToken() {
        sharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");
        if (!token.equals("")) {
            new PostMethod(new CallBackString() {
                @Override
                public void doit(String str) {
                    if (str.equals("true")) {
                        Toast.makeText(LoginActivity.this, "Token chính xác. Đăng nhập thành công", Toast.LENGTH_LONG).show();
                        nextActivity();
                    } else
                        Toast.makeText(LoginActivity.this, "Tài khoản đã được đăng nhập trên thiết bị khác", Toast.LENGTH_LONG).show();
                }
            }).execute(linkServer + checkToken, token);
        }
    }

    private void addAnima() {
        anim_LeftToRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_to_right);
        anim_RightToLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_to_left);
        anim_ZoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);

        editUserName.startAnimation(anim_LeftToRight);
        editPassWord.startAnimation(anim_RightToLeft);
        btnLogin.startAnimation(anim_ZoomIn);
    }

    private void addEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = editUserName.getText().toString().trim();
                String passWord = editPassWord.getText().toString().trim();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userName", userName);
                    jsonObject.put("password", passWord);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (userName.equals("") && passWord.equals("")) {
                    Toast.makeText(LoginActivity.this, "Username or Password is null", Toast.LENGTH_SHORT).show();
                } else {
                    new PostMethod(new CallBackString() {
                        @Override
                        public void doit(String str) {
                            if (str.equals("")) {
                                Toast.makeText(LoginActivity.this, "Username or Password is wrong", Toast.LENGTH_LONG).show();
                            } else {
                                Account account = new Gson().fromJson(str, Account.class);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("userName", account.getUserName());
                                editor.putInt("id", account.getId());
                                editor.putString("token", account.getToken());
                                editor.apply();

                                nextActivity();
                            }
                        }
                    }).execute(linkServer + getLogin, jsonObject.toString());
                }
            }
        });
    }

    private void nextActivity() {
        Intent intent = new Intent(LoginActivity.this, TableActivity.class);
        startActivity(intent);
        finish();
    }

}