package com.example.orderrestaurantapp;

public interface Config {
    String linkServer = "http://192.168.43.169:8080";
    String getLogin = "/login";
    String getListRoom = "/getListRoom";
    String getListTableInRoom = "/getListTableInRoom";
    String getMenu = "/getMenu";
    String getCurrentOrder = "/getCurrentOrder";
    String createOrUpdateOrder = "/createOrUpdateOrder";
    String checkToken = "/checkToken";
    String checkOut = "/checkOut";
    String transfer = "/transfer";
}
