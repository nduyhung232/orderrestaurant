package com.example.orderrestaurantapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable {
    private int id;
    private int staffId;
    private int customerId;
    private int tableId;
    private String timeOrder;
    private String timePay;
    private int status;
    private ArrayList<Dish> dishList = new ArrayList<>();

    public Order() {
    }

    public Order(int id, int staffId, int customerId, int tableId, String timeOrder, String timePay, int status) {
        this.id = id;
        this.staffId = staffId;
        this.customerId = customerId;
        this.tableId = tableId;
        this.timeOrder = timeOrder;
        this.timePay = timePay;
        this.status = status;
    }

    public ArrayList<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(ArrayList<Dish> dishList) {
        this.dishList = dishList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(String timeOrder) {
        this.timeOrder = timeOrder;
    }

    public String getTimePay() {
        return timePay;
    }

    public void setTimePay(String timePay) {
        this.timePay = timePay;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
