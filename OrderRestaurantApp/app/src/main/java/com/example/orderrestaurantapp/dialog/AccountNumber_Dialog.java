package com.example.orderrestaurantapp.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.calback.CallBackFloat;
import com.example.orderrestaurantapp.calback.CallBackInt;

public class AccountNumber_Dialog extends DialogFragment {
    CallBackInt callBack;

    public AccountNumber_Dialog newInstance(CallBackInt callBack) {
        AccountNumber_Dialog quantityDialog = new AccountNumber_Dialog();
        quantityDialog.callBack = callBack;
        return quantityDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.account_number_dialog, null);

        final EditText editAccountNumber = view.findViewById(R.id.editQuantity);

        Button btnCancel = view.findViewById(R.id.btnCancel);
        Button btnOk = view.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int accountNumber = Integer.parseInt(editAccountNumber.getText().toString().trim());
                callBack.doit(accountNumber);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }
}
