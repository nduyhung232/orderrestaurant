package com.example.orderrestaurantapp.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.orderrestaurantapp.R;
import com.example.orderrestaurantapp.model.Dish;

import java.util.List;

/**
 * Created by duyhung on 25/01/2018.
 */

public class DishOrderAdapter extends ArrayAdapter {

    Activity context;
    int resource;
    List<Dish> objects;

    public DishOrderAdapter(@NonNull Activity context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View view = inflater.inflate(this.resource, null);

        Dish dish = this.objects.get(position);

        ImageView img = view.findViewById(R.id.img);
        TextView txtQuantity = view.findViewById(R.id.txtquantity);
        TextView txtDishName = view.findViewById(R.id.txtDishName);

        String quantity = String.valueOf(dish.getQuantity());
        txtDishName.setText(dish.getName());
        txtQuantity.setText(quantity);

        if (dish.getImage() != null) {
            byte[] decodedString = Base64.decode(dish.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            img.setImageBitmap(decodedByte);
        }

        return view;
    }
}
