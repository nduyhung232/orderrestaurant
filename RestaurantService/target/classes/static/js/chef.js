$(document).ready(function () {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/getListOrder",
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data)

            for (var i = 0; i < data.length; i++) {
                var str1 = "<tr class='row'>\n" +
                    "    <td class='col-1'>Bàn " + data[i].tableId + "</td>\n" +
                    "    <td class='col-2'>" + data[i].staffName + "</td>\n" +
                    "    <td class='col-3'>" + data[i].timeOrder + "</td>\n" +
                    "    <td class='col-5'><ol>";
                for (var j = 0; j < data[i].dishList.length; j++) {
                    str1 += "<li>" +
                        "<span class='col-md-2'>" + data[i].dishList[j].name + "" +
                        "<span class='col-md-2'>" + data[i].dishList[j].quantity + "</span>" +
                        "</li>";
                }
                str1 += "</ol></td>" +
                    "<td class='col-1'><span id='order-id' style='display: none'>" + data[i].id + "</span>" +
                    "<button id='btn-done' class='btn btn-success'>Lên Đồ</button>" +
                    "</td>" +
                    "</tr>";

                $("#list-order").append(str1)
            }
        }
    });

    $('#list-order').on('click', '#btn-done', function (event) {
        var orderid = $(this).parent().find("#order-id").text();
        var url = "/orderDone?id=" + orderid;
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: url,
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                location.reload()
            },
            error: function (jqXHR) {
                console.log("error")
            }
        });
    })
})