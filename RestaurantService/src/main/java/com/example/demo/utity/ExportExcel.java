package com.example.demo.utity;

import com.example.demo.dao.OrderSQL;
import com.example.demo.model.Dish;
import com.example.demo.model.Order;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

public class ExportExcel {
    public boolean exportExcelBill(Order order) throws IOException {
        File file = new File("form-bill.xlsx");
        FileInputStream fis = new FileInputStream(file);

        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheet("Invoice");

        Cell cell_orderID = sheet.getRow(4).getCell(5);
        Cell cell_date = sheet.getRow(4).getCell(7);

        cell_orderID.setCellValue(order.getId());
        cell_date.setCellValue(order.getTimePay());

        int rowNum = 15;
        for (Dish dish : order.getDishList()) {
            XSSFRow row = sheet.getRow(rowNum++);

            Cell cell1 = row.getCell(0);
            cell1.setCellValue(dish.getName());
            Cell cell2 = row.getCell(5);
            cell2.setCellValue(dish.getQuantity());
            Cell cell3 = row.getCell(6);
            cell3.setCellValue(dish.getPrice());
            Cell cell4 = row.getCell(7);
            cell4.setCellValue((dish.getPrice() * dish.getQuantity()));
        }

        Cell cell_total = sheet.getRow(30).getCell(7);
        cell_total.setCellValue(order.getTotal());

        try {
            String name_file = order.getId() + ".xlsx".trim();
            File fileout = new File("bill/" + name_file);
            FileOutputStream outputStream = new FileOutputStream(fileout);
            workbook.write(outputStream);
            workbook.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

//    public static void main(String[] args) throws IOException {
//        ExportExcel exportExcel = new ExportExcel();
//        Order order = new Order();
//        OrderSQL orderSQL = new OrderSQL();
//
//        order = orderSQL.getOrderById(34);
//        exportExcel.exportExcelBill(order);
//    }
}
