package com.example.demo.model;

public class Dish {
    private int id;
    private String name;
    private float price;
    private String typeFood;
    private String image;
    private int quantity;

    public Dish(int id, String name, float price, String typeFood, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.typeFood = typeFood;
        this.image = image;
    }

    public Dish(int id, String typeFood, int quantity) {
        this.id = id;
        this.typeFood = typeFood;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Dish() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getTypeFood() {
        return typeFood;
    }

    public void setTypeFood(String typeFood) {
        this.typeFood = typeFood;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", typeFood='" + typeFood + '\'' +
                ", image='" + image + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
