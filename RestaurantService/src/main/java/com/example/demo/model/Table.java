package com.example.demo.model;

import java.util.ArrayList;

public class Table {
    private int id;
    private int roomId;
    private String name;
    private ArrayList listOrder;

    public Table() {
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getListOrder() {
        return listOrder;
    }

    public void setListOrder(ArrayList listOrder) {
        this.listOrder = listOrder;
    }

    public Table(int id, String name, ArrayList listOrder) {
        this.id = id;
        this.name = name;
        this.listOrder = listOrder;
    }

    public Table(int id, int roomId, String name) {
        this.id = id;
        this.roomId = roomId;
        this.name = name;
    }

}
