package com.example.demo.model;

import java.util.ArrayList;

public class Room {
    private int id;
    private String name;
    private ArrayList<Table> listTable;

    public Room() {
    }

    public Room(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Room(int id, String name, ArrayList<Table> listTable) {
        this.id = id;
        this.name = name;
        this.listTable = listTable;
    }

    public ArrayList<Table> getListTable() {
        return listTable;
    }

    public void setListTable(ArrayList<Table> listTable) {
        this.listTable = listTable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
