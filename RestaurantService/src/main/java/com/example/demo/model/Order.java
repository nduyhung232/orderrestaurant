package com.example.demo.model;

import java.util.ArrayList;

public class Order {
    private int id;
    private int staffId;
    private int customerId;
    private int tableId;
    private String timeOrder;
    private String timePay;
    private int status;
    private ArrayList<Dish> dishList = new ArrayList<>();
    private String staffName;
    private int total;

    public Order() {
    }

    public Order(int id, int staffId, int customerId, int tableId, String timeOrder, String timePay, int status, int total) {
        this.id = id;
        this.staffId = staffId;
        this.customerId = customerId;
        this.tableId = tableId;
        this.timeOrder = timeOrder;
        this.timePay = timePay;
        this.status = status;
        this.total = total;
    }

    public Order(int id, int staffId, int customerId, int tableId, String timeOrder, String timePay, int status) {
        this.id = id;
        this.staffId = staffId;
        this.customerId = customerId;
        this.tableId = tableId;
        this.timeOrder = timeOrder;
        this.timePay = timePay;
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public ArrayList<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(ArrayList<Dish> dishList) {
        this.dishList = dishList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(String timeOrder) {
        this.timeOrder = timeOrder;
    }

    public String getTimePay() {
        return timePay;
    }

    public void setTimePay(String timePay) {
        this.timePay = timePay;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", staffId=" + staffId +
                ", customerId=" + customerId +
                ", tableId=" + tableId +
                ", timeOrder='" + timeOrder + '\'' +
                ", timePay='" + timePay + '\'' +
                ", status=" + status +
                ", dishList=" + dishList.toString() +
                '}';
    }
}
