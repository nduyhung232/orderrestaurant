package com.example.demo.dao;

import com.example.demo.dao.config.MyConnectionSql;
import com.example.demo.model.Account;
import com.example.demo.model.Dish;
import com.example.demo.model.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

public class AccountSQL {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public static String createToken(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    public Account checkLogin(String userName, String userPass) {
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from Account WHERE userName = '" + userName
                    + "' AND password = '" + userPass + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                Account account = new Account(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3));

                account.setToken(createToken());
                updateToken(account);

                return account;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateToken(Account account) {
        try {
            Statement statement = connection.createStatement();
            String sql = "update Account set Token = '" + account.getToken() + "' where id = " + account.getId();

            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public Account checkToken(String token) {
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from Account WHERE Token = '" + token + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Account accountDTO = new Account(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4));

                return accountDTO;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}

