package com.example.demo.dao;


import com.example.demo.dao.config.MyConnectionSql;
import com.example.demo.model.Dish;
import com.example.demo.model.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderSQL {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public ArrayList<Dish> getMenu() {
        ArrayList<Dish> listDish = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from order_restaurant.Dish";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Dish dish = new Dish(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getFloat(3),
                        resultSet.getString(4),
                        resultSet.getString(5));
                listDish.add(dish);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listDish;
    }

    public Order getCurrentOrder(int idTable) {
        Order order = new Order();
        try {
            int statusTable = getStatusByIdTableDESCLimit1(idTable);
            if (statusTable == 1 || statusTable == 2) {
                Statement statement = connection.createStatement();
                String sql = "select * from Orders where TableID = " + idTable + " and Status = " + statusTable;
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                    order = new Order(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getInt(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getInt(7));
                }

                order.setDishList(getListDishByOrder(order.getId()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return order;
    }

    public ArrayList<Dish> getListDishByOrder(int orderId) {
        ArrayList<Dish> listDish = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "select Dish.id, Dish.TypeFood, Order_Dish.Quantity from  Order_Dish join Dish " +
                    "on Order_Dish.DishID = Dish.ID " +
                    "where Order_Dish.OrderID = " + orderId;
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Dish dish = new Dish(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getInt(3));

                listDish.add(dish);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listDish;
    }

    public int getStatusByIdTableDESCLimit1(int tableId) {
        int status = 0;
        try {
            Statement statement = connection.createStatement();
            String sql = "select Status from Orders where TableID = " + tableId + " ORDER BY id DESC LIMIT 1";
            ResultSet resultSet = statement.executeQuery(sql);

            if (!resultSet.next()) {
                return status;
            }
            status = resultSet.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public boolean createOrUpdateOrder(Order order) {
        try {
            // set current time
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
            Date date = new Date();

            order.setTimeOrder(dateFormat.format(date));

            if (order.getId() == 0 && order.getDishList().size() == 0) {
                return true;
            } else {

                Statement statement = connection.createStatement();
                if (order.getId() != 0) {       // UPDATE ORDER
                /*
                    delete old order_dish
                 */
                    String delete = "delete from Order_Dish where OrderID = " + order.getId();
                    statement.executeUpdate(delete);

                } else {                        // CREATE NEW ORDER
                    //  create new order
                    String sql1 = "insert into Orders (StaffHumanID, TableID,TimeOrder,status) " +
                            "values (" + order.getStaffId() +
                            "," + order.getTableId() +
                            ",'" + order.getTimeOrder() +
                            "'," + order.getStatus() + ")";
                    statement.executeUpdate(sql1);

                    //  get current orderId
                    String getOrderId = "select id from Orders ORDER BY id DESC LIMIT 1";
                    ResultSet resultSet = statement.executeQuery(getOrderId);
                    resultSet.next();
                    int orderId = resultSet.getInt(1);
                    order.setId(orderId);
                }
                //  create relationship order_dish
                List<Dish> listDish = order.getDishList();
                for (Dish dish : listDish) {
                    String sql2 = "insert into Order_Dish (OrderId, Order_Dish.DishID, Quantity)" +
                            "values (" + order.getId() +
                            "," + dish.getId() +
                            "," + dish.getQuantity() + ")";

                    statement.executeUpdate(sql2);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

//    public int getIdAUTO_INCREMENT_OrderDish() {
//        int id = 0;
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "SELECT AUTO_INCREMENT FROM information_schema.TABLES" +
//                    " WHERE TABLE_SCHEMA = 'order_restaurant'" +
//                    " AND TABLE_NAME = 'Order_Dish'";
//
//            ResultSet resultSet = statement.executeQuery(sql);
//
//            if (!resultSet.next()) {
//                return id;
//            }
//            id = resultSet.getInt(1);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return id;
//    }

    /*
        Get List Order - status == 1
     */

    public ArrayList<Order> getListOrder() {
        ArrayList<Order> orders = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT Orders.id, Orders.CustomerHumanID,Human.Name,Orders.TableID,Orders.TimeOrder,Orders.Status \n" +
                    "FROM order_restaurant.Orders join Human\n" +
                    "ON Human.ID = Orders.StaffHumanID where Orders.status = 1\n";
            ResultSet resultSet1 = statement.executeQuery(sql);

            while (resultSet1.next()) {
                Order order = new Order();
                order.setId(resultSet1.getInt(1));
                order.setCustomerId(resultSet1.getInt(2));
                order.setStaffName(resultSet1.getString(3));
                order.setTableId(resultSet1.getInt(4));
                order.setTimeOrder(resultSet1.getString(5));
                order.setStatus(resultSet1.getInt(6));

                orders.add(order);
            }

            for (Order order : orders) {
                String getListDish = "SELECT Order_Dish.OrderID,Dish.Name,Order_Dish.Quantity" +
                        " FROM Dish join Order_Dish\n" +
                        " on Dish.ID = Order_Dish.DishID\n" +
                        " where Order_Dish.OrderID =" + order.getId();
                ResultSet resultSet2 = statement.executeQuery(getListDish);

                while (resultSet2.next()) {
                    Dish dish = new Dish();
                    dish.setName(resultSet2.getString(2));
                    dish.setQuantity(resultSet2.getInt(3));

                    order.getDishList().add(dish);
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    /*
        Change status 1 -> 2
     */
    public boolean orderDone(String orderId) {
        try {
            Statement statement = connection.createStatement();
            String sql1 = "UPDATE `order_restaurant`.`Orders` SET `Status`='2' WHERE `ID`= '" + orderId + "'";
            statement.executeUpdate(sql1);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean checkOut(Order order) {
        try {
            Statement statement = connection.createStatement();
            // set status
            String sql1 = "UPDATE `order_restaurant`.`Orders` SET `Status`='4' WHERE `ID`= '" + order.getId() + "'";
            statement.executeUpdate(sql1);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
            Date date = new Date();

            // set timePay
            String sql2 = "UPDATE `order_restaurant`.`Orders` SET `TimePay`='" + dateFormat.format(date) + "' WHERE `ID`= '" + order.getId() + "'";
            statement.executeUpdate(sql2);

            // set Total
            String sql3 = "UPDATE `order_restaurant`.`Orders` SET `Total`='" + order.getTimeOrder() + "' WHERE `ID`= '" + order.getId() + "'";
            statement.executeUpdate(sql3);

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Order getOrderById(int idOrder) {
        Order order = new Order();
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from Orders where id = " + idOrder;
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                order = new Order(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getInt(7),
                        resultSet.getInt(8));

            }
            resultSet.close();
            order.setDishList(getListDishByOrder(order.getId()));

            // get detail Dish
            for (int i = 0; i < order.getDishList().size(); i++) {
                String sql1 = "select Dish.Name, Dish.Price from Dish where id = " + order.getDishList().get(i).getId();
                ResultSet resultSet1 = statement.executeQuery(sql1);
                if (resultSet1.next()) {
                    order.getDishList().get(i).setName(resultSet1.getString(1));
                    order.getDishList().get(i).setPrice(resultSet1.getInt(2));
                }
                resultSet1.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return order;
    }

}
