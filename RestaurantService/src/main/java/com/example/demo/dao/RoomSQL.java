package com.example.demo.dao;

import com.example.demo.dao.config.MyConnectionSql;
import com.example.demo.model.Room;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RoomSQL {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public ArrayList<Room> getListRoom() {
        ArrayList<Room> listRoom = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from Room";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Room room = new Room(
                        resultSet.getInt(1),
                        resultSet.getString(2));
                listRoom.add(room);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listRoom;
    }
}
