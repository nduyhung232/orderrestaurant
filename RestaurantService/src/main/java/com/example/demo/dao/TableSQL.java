package com.example.demo.dao;


import com.example.demo.dao.config.MyConnectionSql;
import com.example.demo.model.Table;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TableSQL {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public ArrayList<Table> getListTableInRoom(int roomId) {
        ArrayList<Table> listTable = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from order_restaurant.Table where RoomID = " + roomId;
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Table table = new Table(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3));
                listTable.add(table);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listTable;
    }
}
