package com.example.demo.api;

import com.example.demo.dao.RoomSQL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomService {
    RoomSQL roomSQL = new RoomSQL();

    @GetMapping("/getListRoom")
    public String getListRoom(){

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String JSONObject = gson.toJson(roomSQL.getListRoom());

        return JSONObject;
    }
}