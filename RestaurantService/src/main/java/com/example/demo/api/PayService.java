package com.example.demo.api;

import com.example.demo.dao.OrderSQL;
import com.example.demo.model.Order;
import com.example.demo.model.TransferOb;
import com.example.demo.utity.ExportExcel;
import net.minidev.json.JSONObject;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static org.springframework.http.HttpHeaders.USER_AGENT;

@RestController
public class PayService {

    OrderSQL orderSQL = new OrderSQL();
    ExportExcel exportExcel = new ExportExcel();

    @PostMapping("/checkOut")
    public ResponseEntity checkOut(@RequestBody Order order) throws IOException {
        orderSQL.checkOut(order);
        exportExcel.exportExcelBill(orderSQL.getOrderById(order.getId()));
        return ResponseEntity.ok("true");
    }

    @PostMapping("/transfer")
    public ResponseEntity checkOut_byCard(@RequestBody TransferOb transferOb) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("account_from", transferOb.getAccount_from());
        jsonObject.put("account_to", transferOb.getAccount_to());
        jsonObject.put("amount", transferOb.getAmount());

        String response = sendPost(jsonObject.toJSONString());
        if (response.equals("true")) {
            return ResponseEntity.ok("true");
        }
        return ResponseEntity.ok(null);
    }

    private String sendPost(String body) throws Exception {
        HttpURLConnection con;

        String url = "http://192.168.43.169:8181/transfer";

        byte[] postData = body.getBytes(StandardCharsets.UTF_8);

        URL myurl = new URL(url);
        con = (HttpURLConnection) myurl.openConnection();

        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Java client");
        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.write(postData);
        }

        StringBuilder content;

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {

            String line;
            content = new StringBuilder();

            while ((line = in.readLine()) != null) {
                content.append(line);
            }
        }
        con.disconnect();

        System.out.println(content.toString());

        return content.toString();
    }
}