package com.example.demo.api;

import com.example.demo.dao.TableSQL;
import com.example.demo.model.Room;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TableService {
    TableSQL table = new TableSQL();

    @PostMapping("/getListTableInRoom")
    public String getListTableInRoom(@RequestBody Room room){

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String JSONObject = gson.toJson(table.getListTableInRoom(room.getId()));

        return JSONObject;
    }

}