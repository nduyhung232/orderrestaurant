package com.example.demo.api;

import com.example.demo.dao.OrderSQL;
import com.example.demo.model.Dish;
import com.example.demo.model.Order;
import com.example.demo.model.Table;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class OrderService {
    OrderSQL orderSQL = new OrderSQL();

    @GetMapping("/getMenu")
    public ResponseEntity getListRoom() {
        ArrayList<Dish> dishList = orderSQL.getMenu();
        return ResponseEntity.ok(dishList);
    }

    @PostMapping("/getCurrentOrder")
    public ResponseEntity getCurrentOrder(@RequestBody Table table) {
        Order order = orderSQL.getCurrentOrder(table.getId()) ;
        System.out.println(order.toString());
        return ResponseEntity.ok(order);
    }

    @PostMapping("/createOrUpdateOrder")
    public ResponseEntity createOrUpdate_Order(@RequestBody Order order) {
        orderSQL.createOrUpdateOrder(order);
        return ResponseEntity.ok(order);
    }



}
