package com.example.demo.api;

import com.example.demo.dao.OrderSQL;
import com.example.demo.model.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChefService {
    OrderSQL orderSQL = new OrderSQL();

    @GetMapping("/getListOrder")
    public ResponseEntity login() {
        return ResponseEntity.ok(orderSQL.getListOrder());
    }

    @GetMapping("/orderDone")
    public ResponseEntity orderDone(@RequestParam("id") String orderId){
        orderSQL.orderDone(orderId);
        Order order = new Order();
        return ResponseEntity.ok("true");
    }
}