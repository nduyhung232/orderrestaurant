package com.example.demo.api;

import com.example.demo.dao.AccountSQL;
import com.example.demo.model.Account;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoginService {
    AccountSQL accountSQL = new AccountSQL();

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody Account account) {
        Account account1 = accountSQL.checkLogin(account.getUserName(), account.getPassword());
        if (account1 == null){
            System.out.println("Đăng nhập lỗi");
            return null;
        }else {
            System.out.println("Đăng nhập thành công");
            return ResponseEntity.ok(account1);
        }
    }

    @PostMapping("/checkToken")
    public String checkToken(@RequestBody String token){
        System.out.println(token);
        Account account = accountSQL.checkToken(token);
        if (account == null){
            System.out.println("token khong ton tai");
            return "false";
        }else {
            System.out.println("token ton tai");
            return "true";
        }
    }

}
