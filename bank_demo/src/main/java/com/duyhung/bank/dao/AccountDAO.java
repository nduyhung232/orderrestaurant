package com.duyhung.bank.dao;

import com.duyhung.bank.config.MyConnectionSql;
import com.duyhung.bank.model.Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO {
    static MyConnectionSql myConnectionSql = new MyConnectionSql();
    static Connection connection = myConnectionSql.getConnection();

    public boolean transfer(String fromAccount, String toAccount, double amount) {
        try {
            Account account_from = getCurrentBalanAccountce(fromAccount);
            Account account_to = getCurrentBalanAccountce(toAccount);

            if (account_from == null || account_to == null){
                return false;
            }

            if (account_from.getBalance() - amount < 0){
                return false;
            }

            account_from.setBalance(account_from.getBalance() - amount);
            account_to.setBalance(account_to.getBalance() + amount);

            Statement statement = connection.createStatement();

            String sql1 = "update bank_demo.account set balance = " + account_from.getBalance() + " where account_number = '" + account_from.getAccount_number() + "'";
            statement.executeUpdate(sql1);

            String sql2 = "update bank_demo.account set balance = " + account_to.getBalance() + " where account_number = '" + account_to.getAccount_number() + "'";
            statement.executeUpdate(sql2);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    private Account getCurrentBalanAccountce(String account_number) {
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from bank_demo.account where account_number = '" + account_number + "'";
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                Account account = new Account();
                account.setId(resultSet.getInt(1));
                account.setAccount_number(resultSet.getString(2));
                account.setName(resultSet.getString(3));
                account.setBalance(resultSet.getDouble(4));

                return account;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    public List<Account> getAllAccount() {
        try {
            ArrayList<Account> accountArrayList = new ArrayList<>();

            Statement statement = connection.createStatement();
            String sql = "select * from bank_demo.account";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Account account = new Account();
                account.setId(resultSet.getInt(1));
                account.setAccount_number(resultSet.getString(2));
                account.setName(resultSet.getString(3));
                account.setBalance(resultSet.getDouble(4));

                accountArrayList.add(account);
            }

            return accountArrayList;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
