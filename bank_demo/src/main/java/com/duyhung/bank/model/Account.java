package com.duyhung.bank.model;

public class Account {
    private int id;
    private String account_number;
    private String name;
    private double balance;

    public Account(int id, String account_number, String name, double balance) {
        this.id = id;
        this.account_number = account_number;
        this.name = name;
        this.balance = balance;
    }

    public Account() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
