package com.duyhung.bank.controller;

import com.duyhung.bank.dao.AccountDAO;
import com.duyhung.bank.model.Container;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {
    AccountDAO accountDAO = new AccountDAO();

    @GetMapping("/getAccount")
    public ResponseEntity getAccount() {
        return ResponseEntity.ok(accountDAO.getAllAccount());
    }

    @PostMapping("/transfer")
    public ResponseEntity transfer(@RequestBody Container container) {
        String account_from = container.getAccount_from();
        String account_to = container.getAccount_to();
        if (accountDAO.transfer(account_from, account_to, container.getAmount()) == true) {
            return ResponseEntity.ok("true");
        } else
            return ResponseEntity.ok(null);
    }
}
