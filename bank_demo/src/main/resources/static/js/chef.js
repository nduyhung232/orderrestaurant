$(document).ready(function () {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/getAccount",
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log(data)

            for (var i = 0; i < data.length; i++) {
                var str1 = "<tr class='row'>\n" +
                    "    <td class='col-3'>" + data[i].account_number + "</td>\n" +
                    "    <td class='col-3'>" + data[i].name + "</td>\n" +
                    "    <td class='col-3'>" + data[i].balance + "</td>";

                $("#list-order").append(str1)
            }
        }
    });
})